module gitlab.com/tankyouoss/pubsubwrapper

go 1.14

require (
	cloud.google.com/go/pubsub v1.3.1
	github.com/stretchr/testify v1.4.0
)
